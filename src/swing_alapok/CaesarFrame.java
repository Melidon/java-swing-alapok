package swing_alapok;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

public class CaesarFrame extends JFrame {

	private static final long serialVersionUID = 1L; // csak mert panaszkodott az Eclipse
	private JTextField inputTextField;
	private JTextField outputTextField;
	private JButton button;
	private JComboBox<?> comboBox;
	private boolean upperTextFieldUsedLastTime;

	private void refresh() {

		if (this.upperTextFieldUsedLastTime) {
			
			String input = inputTextField.getText();
			char offset = (char) comboBox.getSelectedItem();
			String output = caesarCode(input, offset);
			outputTextField.setText(output);
			
		} else {
			
			String input = outputTextField.getText();
			char offset = (char) comboBox.getSelectedItem();
			String output = caesarDecode(input, offset);
			inputTextField.setText(output);
			
		}

	}

	private class OkButtonActionListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {

			refresh();

		}

	}

	private class InputFieldDocumentListener implements DocumentListener {

		@Override
		public void changedUpdate(DocumentEvent e) {

		}

		@Override
		public void insertUpdate(DocumentEvent e) {

			refresh();

		}

		@Override
		public void removeUpdate(DocumentEvent e) {

			refresh();

		}

	}

	private class InputFieldFocusListener implements FocusListener {

		private boolean isUpper;

		public InputFieldFocusListener(boolean isUpper) {
			this.isUpper = isUpper;
		}

		@Override
		public void focusGained(FocusEvent e) {

			upperTextFieldUsedLastTime = this.isUpper;

		}

		@Override
		public void focusLost(FocusEvent e) {

			upperTextFieldUsedLastTime = this.isUpper;

		}

	}

	public CaesarFrame() {

		// 2. részfeladat
		this.setDefaultCloseOperation(CaesarFrame.EXIT_ON_CLOSE);
		this.setTitle("SwingLab");
		this.setSize(400, 110);
		this.setResizable(false);

		// 3. részfeladat
		Object[] abc = new Character['Z' - 'A' + 1];
		for (char ch = 'A'; ch <= 'Z'; ++ch) {
			abc[ch - 'A'] = ch;
		}
		this.comboBox = new JComboBox<Object>(abc);
		this.inputTextField = new JTextField(20);
		this.outputTextField = new JTextField(20);
		this.button = new JButton("Code!");
		JLabel text = new JLabel("Output:");
		JPanel upper = new JPanel();
		JPanel lower = new JPanel();
		this.outputTextField.setEditable(false);
		upper.add(this.comboBox);
		upper.add(this.inputTextField);
		upper.add(this.button);
		lower.setLayout(new BoxLayout(lower, BoxLayout.X_AXIS));
		lower.add(text);
		lower.add(this.outputTextField);
		JPanel wrapper = new JPanel();
		wrapper.setLayout(new BorderLayout());
		wrapper.add(upper, BorderLayout.NORTH);
		wrapper.add(lower, BorderLayout.SOUTH);
		this.add(wrapper);
		this.setResizable(true);
		this.pack();

		// 4. részfeladat
		this.button.addActionListener(new OkButtonActionListener());

		// 6. részfeladat
		//this.inputTextField.getDocument().addDocumentListener(new InputFieldDocumentListener());

		// 7. részfeladat
		this.outputTextField.setEditable(true);
		this.inputTextField.addFocusListener(new InputFieldFocusListener(true));
		this.outputTextField.addFocusListener(new InputFieldFocusListener(false));

	}

	private String caesarCode(String input, char offset) {

		input = input.toUpperCase();
		offset -= 'A';
		StringBuffer result = new StringBuffer();
		for (int i = 0; i < input.length(); i++) {
			char ch = (char) ((((input.charAt(i) + offset) - 'A') % ('Z' - 'A' + 1)) + 'A');
			result.append(ch);
		}
		return result.toString();

	}
	
	private String caesarDecode(String input, char offset) {
		offset = (char) ('Z' + 1 - offset + 'A');
		return caesarCode(input, offset);
	}

}
